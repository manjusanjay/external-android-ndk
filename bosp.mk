
ifdef CONFIG_EXTERNAL_ANDROID-NDK

# Targets provided by this project
.PHONY: android-ndk clean_android-ndk

# Add this to the "external" target
external: android-ndk
clean_external: clean_android-ndk


# Android NDK Configuration
BOSP_ANDROID_NDK ?= "http://bosp.dei.polimi.it/files/android-ndk-bosp.tar.bz2"
BOSP_ANDROID_NDK_MD5 := 8737d761c45128b5b0aacde0029165cb
BOSP_ANDROID_PLATFORM := 9
BOSP_ANDROID_TOOLCHAIN_DIR := $(BUILD_DIR)/android-toolchain

android-ndk: external/android-ndk/install
external/android-ndk/install:
	@echo
	@echo "==== Installing Standalone Android NDK Toolchain ===="
	@echo "Downloading Android NDK [$(BOSP_ANDROID_NDK)]...";
	@cd external \
		&& [ -f android-ndk-bosp.tar.bz2 ] \
		|| wget $(BOSP_ANDROID_NDK) \
		|| exit 1;
	@echo "Uncompressing NDK...";
	@cd external && tar xjf android-ndk-bosp.tar.bz2 || exit 1;
	@echo "Installing Standalone Toolchain...";
	@cd external/android-ndk && ./build/tools/make-standalone-toolchain.sh \
		--platform=android-$(BOSP_ANDROID_PLATFORM) \
		--install-dir=$(BOSP_ANDROID_TOOLCHAIN_DIR)
	@ln -s $(BOSP_ANDROID_TOOLCHAIN_DIR) external/android-ndk/install

clean_android-ndk:
	@echo
	@echo "==== Clean-up Standalone Android NDK Toolchain ===="
	@rm -rf $(BOSP_ANDROID_TOOLCHAIN_DIR) || true
	@rm -f external/android-ndk/install
	@echo

else # CONFIG_EXTERNAL_ANDROID-NDK

android-ndk:
	$(warning external/android-ndk module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_ANDROID-NDK

